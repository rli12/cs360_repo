#include "colors.inc"    // The include files contain. Without this, just black screen

camera { orthographic  sky < 0, 1, 0 >  look_at < 0, 2, 1 > }
light_source { < 1, 2, 0 >, color  rgb < 0.3, 0.5, 0.2 > spotlight  radius 10 }
sphere { < 2, 1, 1 >, 1.5  texture { pigment {  color  rgb < 0.3, 0.5, 0.2 > } } }    
plane { <0, 1, 0>, 4 texture { pigment { checker White , Yellow } } }