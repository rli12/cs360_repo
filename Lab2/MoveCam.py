#!Python3   MoveCam.py

import math # for calculate (x, y) point on the moving path
import re   # built-in package that can be used to work with regular expressions

# Take angle degrees and convert to radians, and apply cos(radians) to get vector x; 2 floats
def getXvalue(degree):
    return str(round(4 * math.cos(math.radians(int(degree))),2))

# Take angle degrees and convert to radians, and apply sin(radians) to get vector y; 2 floats
def getYvalue(degree):
    return str(round(4 * math.sin(math.radians(int(degree))),2))


#xpattern = r"(?<=<).*?(?=,)"   # x
#ypattern = r"(?<=,).*?(?=,)"   # y
def updateValue(pattern, deg, oldvalue):
    return re.sub(pattern, deg, oldvalue)


