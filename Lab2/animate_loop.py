#!python3
import sys  #interact with the interpreter
import os   #.system() function to invoke a program
import re   # built-in package that can be used to work with regular expressions

import math # for calculate (x, y) point on the moving path
import MoveCam

import Ob1


def prepFile( filename ):
    fin = open(filename)
    # Read the entire file into a string
    sdl = fin.read()
    fin.close()
    return sdl

# Search Value by regext
def searchValue( regex , wholeString):
    search_value = str(re.search( regex , wholeString ))
    return search_value


def main():

    #------------The POV-RAY code goes here as one string----------------------------------------------
    # Use python to search-find-match and change the code in this section.
    # First line in tmp.pov in order to seperate with base.pov
    sdl_new = "//This is tmp.pov" + "\n"
    sdl_new += prepFile('base.pov')


    # Search Camera location
    #search_value = str(re.search("(?<=location ).+", sdl_new))  #Get everyting after location.    value = " <0, 2, -3>"
    search_value = searchValue("(?<=location ).+", sdl_new)
    
    # Refine search value <_src.SRE_Matchobject;span=(413,423),match='<0,2,-3>'> 
    search_value = search_value.replace(' ', '')  #Take out spaces.     value = "<0,2,-3>"
    pattern = r"(?<=match=').+\b>"
    #newValue = re.findall(pattern, value)    # ['<0,2,-3>']
    value = re.findall(pattern, search_value)[0]  # <0,2,-3>


    #-------------------------------LOOP----------LOOP------------------------------------------------------
    
    
    # Start generating picture and number them start 1
    picNo = 1 
    # Turnning degree for calculate camera moving path 
    degree = 0

    while picNo < 11:
        # turn evenly
        degree += int(360/11)

        # update x <1,2,-3>
        value = MoveCam.updateValue(r"(?<=<).*?(?=,)", MoveCam.getXvalue(str(degree)), value)
        # update y <1,4,-3>
        value = MoveCam.updateValue(r"(?<=,).*?(?=,)", MoveCam.getYvalue(str(degree)), value)
        new_value = value


        # Take everything after location except new line and substitude by newValue  --next step--> make a function return sdl_new
        pattern = r"(?<=location ).+"
        sdl_new = re.sub(pattern, new_value, sdl_new)

        #print(new_value)
        #--------------------------------Code updated successfully------------------------------------------
        #------------------Working on img---------------------

        # Initial new file
        outfile = 'tmp.pov'
        # Open the new file for writing only. Overwrite exist and create non-exist
        fout = open(outfile, 'w')
        fout.write( sdl_new )
        fout.close()
        #sys.stdout.write(sdl)

        #-----from the command line, execute the os.system() function to invoke a program.------------------
        '''
        +-D  Turns graphic display on/off
        +-V  Set verbose message on/off
        +-A  Turns aa on with threshod 0.3 or previous amount/ Turns anti-aliasing off
        Height=600
        Width=800
        '''
        pov_cmd = 'pvengine.exe  +I%s +O%s -D -V +A +H600 +W800 /EXIT'

        '''
        zfill(n) fill the string with zeros until it is n characters long
        command to execute pov file in command line % (take the pov file, concatenate the name of rendered img)
        '''
        #cmd = pov_cmd % (outfile, "tmp" + str("picNo").zfill(4) + ".png")
        # temp001.png
        cmd = pov_cmd % (outfile, "tmp" + str(picNo) + ".png")
        os.system(cmd)
        # Increment Picture number
        picNo += 1

        # The idea for this part is to render multiple images by using a loop
        #---------------------------------------------------------------------------------------------------




    #
    # Now make the movie
    #
    print ('Encoding movie')
    #os.system('mencoder.exe mf://tmp*.png -mf type=png:fps=25 -ovc lavc -lavcopts vcode=msmpeg4:vbitrate=2160000:keyint=5:vhq -o movie.avi')
    #-r 30      to force frame rate of the output file to 30 fps
    #-pix_fmt yuv420p     for output to work in QuickTime and most other players
    os.system('ffmpeg -start_number 1 -i tmp%d.png -c:v libx264 -r 30 -pix_fmt yuv420p movie.avi')

if __name__ == "__main__":
    main()