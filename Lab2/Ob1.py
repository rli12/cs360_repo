#!python3
# Lab_2_Part2
# Use one example object from isosurfaces.pov (Copied the isosurfaces.pov and save as OBisosurfaces.pov under Lab2 folder)
import sys  #interact with the interpreter
import os   #.system() function to invoke a program
import re   # built-in package that can be used to work with regular expressions

import animate_loop
'''
def prepFile( filename ):
    fin = open(filename)
    # Read the entire file into a string
    sdl = fin.read()
    fin.close()
    return sdl
'''

def createOB():
    obString = prepFile("OBisosurfaces.pov")
    print(obString)


def main():
    createOB()



if __name__ == "__main__":
    main()