#include "colors.inc"    // The include files contain
    


//background
background { rgb < 0.01, 0.02, 0.08 > }


//The camera statement describes where and how the camera sees the scene.
//It gives x-, y- and z-coordinates to indicate the position of the camera
//and what part of the scene it is pointing at. We describe the coordinates
//using a three-part vector.   
camera {
    location <0, 2, -3>
    look_at  <0, 1.4,  0>
  }  
  
  
//Defining a Light Source
light_source { <2, 4, -2> rgb < 0.8, 1, 0.7 >}      
      
      
     
              
   
        union {
           
           
                        // The Earth with ocean and land
                        difference { 
                                //Place a sphere into the scene as the Earth.
                                sphere {
                                        <0, 0.6, 1>, 1.2
                                        texture {
                                                 pigment { color rgb < 0.25, 0.25, 0.9 > }       //short way:  rgb <0.25, 0.25, 0.9>
                                                 finish { brilliance 3
                                                          roughness 5
                                                 }
                                        }
                                }
                                
                        
                                //As the land on the Earth.
                                julia_fractal { <-0.14,0.5, -0.55, -0.62> 
                                                quaternion 
                                                sqr
                                                max_iteration 10
                                                precision 21 
                                                texture { 
                                                        pigment { color <0.5, 0.7, 0.4> } 
                                                }
                                } 
                                
                                //As the land on the Earch
                                julia_fractal { <0.3,-0.2, -0.65, -0.4> 
                                                quaternion 
                                                sqr
                                                max_iteration 9
                                                precision 18 
                                                texture { 
                                                        pigment { color <0.5, 0.7, 0.4> } 
                                                }
                                }
                        }    
        
        
                        // A Planate
                        sphere {
                                <3, 2, 5>, 0.8
                                texture {
                                         pigment { color rgb < 0.7, 0.5, 0.2 > }       
                                         finish { brilliance 2 
                                                  irid { 1.5 turbulence 6 }
                                         }
                                }
                        }   
                                       
                            
                        // UFO    
                        union{   
                                // Body of UFO   
                                intersection {    
                                                blob { threshold 4
                                                       sphere { <-1, 2.9, 1>, 2.5, strength 4.5
                                                                texture { 
                                                                        pigment { color Brown }
                                                                }
                                                       }            
                                                } 
                                                
                                                blob { threshold 4
                                                       sphere { <-1, 2, 1>, 2.5, strength 4.5
                                                                texture { 
                                                                        pigment { color White }
                                                                }
                                                       }            
                                                }       
                                }
                                
                                // Cap of UFO
                                sphere {
                                        <-1, 2.55, 1>, 0.15
                                        texture {
                                                 pigment { color rgb < 0.05, 0.5, 0.2 > }       
                                                 finish { brilliance 4 
                                                          irid { 1.5 turbulence 12 }
                                                 }
                                                 normal { hexagon 13 }
                                        }
                                } 
                        
                          }
        }  
        
        

 
  
  
