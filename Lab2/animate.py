#!python3
import sys  #interact with the interpreter
import os   #.system() function to invoke a program
import re   # built-in package that can be used to work with regular expressions

import math # for calculate (x, y) point on the moving path
import mymodule


fin = open('base.pov')
# Read the entire file into a string
sdl = fin.read()

fin.close()

#------------The POV-RAY code goes here as one string----------------------------------------------
# Use python to search-find-match and change the code in this section.
sdl_new = "//This is a new string in tmp.pov" + "\n"
sdl_new += sdl



# Search Camera location
search_value = str(re.search("(?<=location ).+", sdl_new))  #Get everyting after location.    value = " <0, 2, -3>"
search_value = search_value.replace(' ', '')  #Take out spaces.     value = "<0,2,-3>"
# Take a charactor after < and substitued by 1


# Get newValue   --next step--> make a function return newValue
pattern = r"(?<=match=').+\b>"
#newValue = re.findall(pattern, value)    # ['<0,2,-3>']
value = re.findall(pattern, search_value)[0]  # <0,2,-3>


# update x
value = mymodule.updateValue(r"(?<=<).*?(?=,)", mymodule.getXvalue("90"), value)
# update y
value = mymodule.updateValue(r"(?<=,).*?(?=,)", mymodule.getYvalue("90"), value)

# update z
#value = mymodule.updateValue()

new_value = value


# Take everything after location except new line and substitude by newValue  --next step--> make a function return sdl_new
pattern = r"(?<=location ).+"
sdl_new = re.sub(pattern, new_value, sdl_new)

#print(new_value)
#--------------------------------Code updated successfully------------------------------------------
#------------------Working on img---------------------

# Initial new file
outfile = 'tmp.pov'
# Open the new file for writing only. Overwrite exist and create non-exist
fout = open(outfile, 'w')
fout.write( sdl_new )
fout.close()
#sys.stdout.write(sdl)

#-----from the command line, execute the os.system() function to invoke a program.------------------
'''
+-D  Turns graphic display on/off
+-V  Set verbose message on/off
+-A  Turns aa on with threshod 0.3 or previous amount/ Turns anti-aliasing off
Height=600
Width=800
'''
picNo = 1
pov_cmd = 'pvengine.exe  +I%s +O%s -D -V +A +H600 +W800'

'''
zfill(n) fill the string with zeros until it is n characters long
command to execute pov file in command line % (take the pov file, concatenate the name of rendered img)
'''
#cmd = pov_cmd % (outfile, "tmp" + str("picNo").zfill(4) + ".png")
# temp001.png
cmd = pov_cmd % (outfile, "tmp" + str(picNo).zfill(3) + ".png")
os.system(cmd)


# The idea for this part is to render multiple images by using a loop
#---------------------------------------------------------------------------------------------------