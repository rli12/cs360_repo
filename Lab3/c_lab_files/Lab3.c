/* CS315 Lab 3: C data types */

#include <stdio.h>
#include <stdlib.h>
// declare with constants
#define minSize 1
#define maxSize 10485760

	// define fp
	FILE *fp;

	// declare functions befor main
	// function initialize
	void initialize(FILE **f, int argc, char ** argv)
	{
		if( argc != 2 )
		{
			printf( "Usage: %s filename.mp3\n", argv[0] );
			exit(EXIT_FAILURE);
		}
	
		*f = fopen(argv[1], "rb");
		if( f == NULL )
		{
			printf( "Can't open file %s\n", argv[1] );
			exit(EXIT_FAILURE);
		}
	}



// readfile function read file into memory
double readFile(FILE * f, unsigned char ** data)
{
	// How many bytes are there in the file?  If you know the OS you're
	// on you can use a system API call to find out.  Here we use ANSI standard
	// function calls.
	double size = 0;
	fseek( f, 0, SEEK_END );		// go to 0 bytes from the end
	size = ftell(f);				// how far from the beginning?
	rewind(f);						// go back to the beginning
	
	// use declared constants
	if( size < minSize || size > maxSize )
	{
		printf("File size is not within the allowed range\n"); 
		//Clean up
		fclose(f);
		exit(EXIT_SUCCESS);
	}
	
	printf( "File size: %2f MB\n", size/1048576 );  // display size like 1M
	// Allocate memory on the heap for a copy of the file
	*data = (unsigned char *)malloc(size);
	// Read it into our block of memory which is pointed by data
	size_t bytesRead = fread( *data, sizeof(unsigned char), size, f );
	if( bytesRead != size )
	{
		printf( "Error reading file. Unexpected number of bytes read: %d\n",bytesRead );
		// free memory
		free(data);
		//Clean up
		fclose(f);
		exit(EXIT_SUCCESS);
	}
	return size;
}


struct mp3 {
	char header[4];
	char title[31];
	char artist[31];
	char album[31];
	char year[5];
	char comment[31];
	char copy[2];
	char origin[2];
	char frequency[3];
	char bitrate[5];
};


int main( int argc, char ** argv )
{
	// call function initialize  
	initialize(&fp, argc, argv); 
	if(fp == NULL)
	{
		return EXIT_FAILURE;
	}
	unsigned char * data = NULL;
	double size = readFile(fp, &data);

	
	// We now have a pointer to the first byte of data in a copy of the file, have fun
	// unsigned char * data    <--- this is the pointer
	struct mp3 info;
	fseek(fp, -128*sizeof(char), SEEK_END);
	fread(info.header,sizeof(char),3,fp);

	if(info.header[0] == 'T' && info.header[1] == 'A' && info.header[2] == 'G')
	{
		fread(info.title, sizeof(char),30,fp);
		FILE *fj = NULL;
		initialize(&fj, argc, argv);
		if(fj == NULL){
			return EXIT_FAILURE;
		}
		fseek(fj,17*sizeof(char),SEEK_END);
	}
	
	
 /*Clean up
END:
	fclose(fp);				// close and free the file
	exit(EXIT_SUCCESS);		// or return 0;
	*/
}
