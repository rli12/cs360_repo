{-  Lab 4: Runnan Li -}


-- Problem #1 gallons, usd, and price
gallons :: Fractional a => a -> a
gallons g = 0.264*g
usd :: Fractional a => a -> a
usd x = 0.75*x
-- Use these two functions to answer the question
price:: Fractional a => a -> a -> a
price us g = (usd us)/(gallons g)

{-ANSWER: 3.5750766087844745   -}


-- Problem #2 flight distance
radians::(Floating a, Floating b)=>(a,b) -> (a, b)
radians (x, y)=((pi*x)/180, (pi*y)/180)
-- What is the flight distance between the point at 45N, 122W to the point 21 N, 158 W
distance:: Floating a => (a, a) -> (a, a) -> a
distance (x1, y1) (x2, y2) = 3963*acos(cos(rx1)*cos(rx2)*cos(ry1-ry2)+sin(rx1)*sin(rx2))
    where (rx1, ry1)=radians(x1, y1)
          (rx2, ry2)=radians(x2, y2)
{-ANSWER: 2631.8416987553624   -}

-- Problem #3 the sum of the cubes of all the  odd numbers between 1000 and2000
oddSum :: Integer
oddSum = sum[1001,1003..2000]
{-ANSWER: 750000               -}

-- Problem #4 Write expressions using map, filter, any, orall to
--Remove all spaces from a string
rmSpace::[Char] -> [Char]
rmSpace s = filter(/=' ') s


--filter out all even numbers from a list (use the even function)
evenNums::Integral a => [a] -> [a]
evenNums [] = []
evenNums (x:xs) 
    | even x = x: evenNums xs
    | otherwise = evenNums xs
{-Answer: [0,2,4,6,8,10]        -}   

--double every value in a list
doubleList :: Num a => [a] -> [a]
doubleList n = map(\n-> n*2) n
{-ANSWER: [8,10,16]             -} 

--tell you True or False if a list contains the number 55
check55 :: (Foldable t, Eq a, Num a) => t a -> Bool
check55 x = any(55==) x
{-ANSWER: True                  -}

--tell you True or False if all the values in a list are odd (use the odd function)
checkOdd :: (Foldable t, Integral a) => t a -> Bool
checkOdd x = all odd x
{-ANSWER: True                   -}

--Write a function called isPrime that determines if an Integer is a prime number
--check if the given number is a prime number
isPrime :: Integral a => a -> Bool
isPrime n = length [ x | x <- [2..n], mod n x == 0] == 1
--get the first
getFirst :: Integral a => Int -> [a]
getFirst n = take n ([x|x<-[2,3..],isPrime x==True])
--get the last
getLast :: Integral a => Int -> Int -> [a]
getLast n m = reverse(take m (reverse (getFirst n)))
{-ANSWER: [7919,7927,7933,7937,7949,7951,7963,7993,8009,8011,8017,8039,8053,8059,8069,8081,8087,8089,8093,8101,8111]   -}


   
--Problem #6 prime factorization
primeFactors :: Integer -> [Integer]
primeFactors x = let (f, f1) = factorP x
                     f' = if prime f then [f] else primeFactors f
                     f1' = if prime f1 then [f1] else primeFactors f1
                 in f' ++ f1'
 where
 factorP x = let f = head $ factors x
                  in (f, x `div` f)
 factors x    = filter (isFactor x) [2..x-1]
 isFactor x y = mod x y == 0
 prime x      = null $ factors x
 
--remove the duplicate numbers
rmDupl :: Eq a => [a] -> [a]
rmDupl [] = []
rmDupl (n:ns)
    | n `elem` ns = rmDupl ns
    | otherwise = n : rmDupl ns
--call factor
uniqueFactors x =rmDupl(primeFactors x)
{-ANSWER: 
    uniqueFactors 175561 = [419]     
    uniqueFactors 62451532000 = [2,5,11,13,23,47,101]
-}





--Problem #7 print out all the result of the functions above
main = do
        --P1 
        print(price 62.3 78.4)
        
        --P2
        print(distance (45,122)(21,158))
 
        --P3
        print(oddSum)
        
        --P4
        print(rmSpace "Hello World! Runnan Li")
        print(evenNums [0,1,2,3,4,5,6,7,8,9,10])
        print(doubleList [4,5,8])
        print(check55 [1,55,45])
        print(checkOdd [1,3,5])
        
        --P5
        print (getLast 1020 21)
        
        
        --P6
        print(uniqueFactors 175561)
        print(uniqueFactors 62451532000)
        
    