import System.IO
import Data.List
import Data.Char

--main=do{

--let x = 818281336460929553769504384519009121840452831049
getSqureRoot x = sqrt(fromIntegral x)
--passing in 818281336460929553769504384519009121840452831049 evaluates to its squre root: 9.045890428592033e23

--let y = 'A'
asciiChar y = chr(ord y -1)
--passing in 'A' evaluates to '@'

isEven x
    | mod (3*x+1) 2 == 0 = True
    | otherwise = False
--passing in 13 evaluates to True, 14 evaluates to False

allOdd = [1, 3..100]
--multOfList = foldl (*) 1 allOdd
productNum = product allOdd
--passing allOdd list from 1 - 100 evaluates 2725392139750729502980713245400918633290796330545803413734328823443106201171875

exList = [99, 23, 4, 2, 67, 82, 49, -40]
getHead = head exList
getTail = last exList
exList1 = drop 1 exList
exListMiddle = take 6 exList1
--exListSortMiddle = sort exListMiddle
getMax = maximum exListMiddle
--passing exList drop head and take 6, and call maximum evaluates 82

constructList a b c d = a:b:c:d:[]
--passing elements 6 19 41 (-3) evaluates to [6,19,41,-3]

even27 = take 27 [0,2..]
--passing infinit even list and take 27 elements from the list evaluates [0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,40,42,44,46,48,50,52]

oddLess200 = [x | x <- [1,3..300], x<200, mod x 3 == 0, mod x 7 == 0]
--passing odd list and filter elements that less then 200 that can devided by 3 and 7 evaluates [21,63,105,147,189]

oddNumbers = [x | x <- [100, 101..200], (x+1) `mod` 2 == 0, mod x 9 == 0]
counts = length oddNumbers
--passing list of 100-200 and find odd elements that can be devided by 9 evaluates [117,135,153,171,189]
--passing oddNumbers to get the length of the list evaluates 5


countNeg y = length [x | x <- y, x < 0]
--passing a list [-1, -3, 4, 9, -6] and count the number of negative elements evaluates 3

decimalV = [0,1..15]
hexV = ['0', '1'..'9']
hexChar = ['A', 'B'..'F']
hexList = hexV ++ hexChar
hexMap = zip decimalV hexList
--using zip to create hexMap evaluates [(0,'0'),(1,'1'),(2,'2'),(3,'3'),(4,'4'),(5,'5'),(6,'6'),(7,'7'),(8,'8'),(9,'9'),(10,'A'),(11,'B'),(12,'C'),(13,'D'),(14,'E'),(15,'F')]


makeList x = [1..x]
makeListNew z = [makeList y | y <- [1..z]]
--passing a number 3 to generate a list of Lists according to the number evaluates to [[1],[1,2],[1,2,3]]
    
replaceSpace url = [ whiteSpace | stringChar <- url, whiteSpace <- if (stringChar == ' ') then "%20" else [stringChar] ]
--passing in "http://wou.edu/my homepage/I love spaces.html" to replace white space with "%20" evaluates to "http://wou.edu/my%20homepage/I%20love%20spaces.html"


getSuit x = case x of
    0 -> "Heart"
    1 -> "Diamond"
    2 -> "Spade"
    3 -> "Club"
    _ -> "error"
--passing a number 2 return a related string evaluates to "Spade"


dotProduct (x, y, z) (a, b, c) = x*a + y*b + z*c
--passing two 3D vectors (1,2,3.0) (4.0,5,6) and get dot product evaluates to 32.0


--reverseFirstThree :: [a] -> [a]
reverseFirstThree plainList = reverse(take 3 plainList) ++ (drop 3 plainList)
--passing a list [1,2,3,4,5] and take first three to reverse return a whole new list evaluates to [3,2,1,4,5]


feelsLike :: Double -> String
feelsLike x
    | (x >= 0.0) && (x < 32.0) = "Below Zero!"
    | (x >= 32.0) && (x < 68.0) = "Getting warm!"
    | (x >= 68.0) && (x < 100.0) = "Getting hot!"
    | (x >= 100.0) = "Super hot outside!"
    | (x == (-45.3)) = "Frostbite central!" 
--passing Fahrenheit return a string.


--feelLike2 :: Double -> (Double, String)
feelsLike2 x
    | (x >= (-17.8)) && (x < 0.0) = ((x*(180/100)+32), "Below Zero!")
    | (x >= 0.0) && (x < 20.0) = ((x*(180/100)+32), "Getting warm!")
    | (x >= 20.0) && (x < 37.8) = ((x*(180/100)+32), "Getting hot!")
    | (x >= 37.8) = ((x*(180/100)+32), "Super hot outside!")
    | (x == (-20)) = ((x*(180/100)+32), "Frostbite central!") 
--passing celsius return a fahrenheit with a string.



cylinderToVolume [ (x1, y1), (x2, y2), (x3, y3), (x4, y4)] = [(3.14*(x1)^2*y1), (3.14*(x2)^2*y2), (3.14*(x3)^2*y3), (3.14*(x4)^2*y4)]
--passing a list of cylinders [(2,5.3),(4.2,9),(1,1),(100.3,94)] and return a list of volumes evaluates to [66.568,498.5064,3.14,2969336.1644]


--}

main=do
        --Q1
        print(getSqureRoot 818281336460929553769504384519009121840452831049)
        --passing in 818281336460929553769504384519009121840452831049 evaluates to its squre root: 9.045890428592033e23
    
        --Q2
        print(asciiChar 'A')
        --passing in 'A' evaluates to '@'

        --Q3
        print(isEven 13)
        --passing in 13 evaluates to True, 14 evaluates to False

        --Q4
        print(productNum)
        --passing allOdd list from 1 - 100 evaluates 2725392139750729502980713245400918633290796330545803413734328823443106201171875

        --Q5
        print(getMax)
        --passing exList drop head and take 6, and call maximum evaluates 82
        
        --Q6
        print(constructList 6 19 41 (-3))
        --passing elements 6 19 41 (-3) evaluates to [6,19,41,-3]

        --Q7
        print(even27)
        --passing infinit even list and take 27 elements from the list evaluates [0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,40,42,44,46,48,50,52]

        --Q8
        print(oddLess200)
        --passing odd list and filter elements that less then 200 that can devided by 3 and 7 evaluates [21,63,105,147,189]

        --Q9
        print(counts)
        --passing oddNumbers to get the length of the list evaluates 5

        --Q10
        print(countNeg [-1, -3, 4, 9, -6])
        --passing a list [-1, -3, 4, 9, -6] and count the number of negative elements evaluates 3

        --Q11
        print(hexMap)
        --using zip to create hexMap evaluates [(0,'0'),(1,'1'),(2,'2'),(3,'3'),(4,'4'),(5,'5'),(6,'6'),(7,'7'),(8,'8'),(9,'9'),(10,'A'),(11,'B'),(12,'C'),(13,'D'),(14,'E'),(15,'F')]

        --Q12
        print(makeListNew 3)
        --passing a number 3 to generate a list of Lists according to the number evaluates to [[1],[1,2],[1,2,3]]

        --Q13
        print(replaceSpace "http://wou.edu/my homepage/I love spaces.html")
        --passing in "http://wou.edu/my homepage/I love spaces.html" to replace white space with "%20" evaluates to "http://wou.edu/my%20homepage/I%20love%20spaces.html"

        --Q14
        {-

        (*) :: Num a => a -> a -> a
        x = 3 * 4

        min :: Ord a => a -> a -> a
        min 3 4
        return 3

        take :: Int -> [a] -> [a]
        take 3 [4,5,6,7,8,9]
        return [4,5,6]

        length :: Foldable t => t a -> Int
        length [1,2,3,4]
        return 4
        
        head :: [a] -> a
        head [2,3,4]
        return 2
        
        -}



        --Q15
        print(getSuit 2)
        --passing a number 2 return a related string evaluates to "Spade"

        --Q16
        print(dotProduct (1,2,3.0) (4.0,5,6))
        --passing two 3D vectors (1,2,3.0) (4.0,5,6) and get dot product evaluates to 32.0

        --Q17
        print(reverseFirstThree [1,2,3,4,5])
        --passing a list [1,2,3,4,5] and take first three to reverse return a whole new list evaluates to [3,2,1,4,5]

        --Q18
        print(feelsLike 60)
        --passing Fahrenheit 60 return a string "Getting warm!"

        --Q19
        print(feelsLike2 25)
        --passing celsius 25 return a fahrenheit with a string "Getting hot!"

        --Q20
        print(cylinderToVolume [(2,5.3),(4.2,9),(1,1),(100.3,94)])
        --passing a list of cylinders [(2,5.3),(4.2,9),(1,1),(100.3,94)] and return a list of volumes evaluates to [66.568,498.5064,3.14,2969336.1644]
        
