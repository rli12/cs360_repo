
module Assn2b (
ownLength,
convertIntToStringLeft,
convertIntToStringRight,
rewriteOne,
rewriteTwo
) where
import Data.Char


--Q2
ownLength :: [a] -> Int
ownLength = foldr (\_ n -> 1 + n) 0 


--Q3
convertIntToStringLeft :: [Int] -> [Char]
convertIntToStringLeft x =foldl (\acc x -> acc ++ [intToDigit x]) [] x
convertIntToStringRight :: [Int] -> [Char]
convertIntToStringRight x =foldr (\x xs->intToDigit x : xs) [] x


--Q4
rewriteOne = length $ filter (<20) [1..100]
rewriteTwo = take 10 $cycle $filter (>5) $ map (*2) [1..10]