import System.IO
import Data.List

import System.Environment (getArgs)
import Assn2b


--Q1
gcdMine :: Integral a => a -> a -> a
gcdCheck x y =(myAnswer, correctAnswer, comment)
 where 
  myAnswer =gcdMine x y
  correctAnswer = gcd x y
  comment =if myAnswer ==correctAnswer then "Matches" else "Does not Match."
gcdMine a b
    | b == 0     = abs a
    | otherwise  = gcdMine b (mod a b)

--Q2
fibonacci :: Int -> Int
fibonacci n
    | n == 1 = 1
    | n == 0 = 0
    | n > 1 = fibonacci(n-1)+fibonacci(n-2)

--Q3
count :: (Eq a, Num b) => a -> [a] -> b
count numList [] = 0
count numList (n:ns)
    | numList==n = 1+(count numList ns)
    | otherwise = count n ns

--Q4
sanitize [] =[]
sanitize (x:xs)
    | x==' ' = "%20"++sanitize xs
    | otherwise = x : sanitize xs

--Q5
multiByTen n =n*10
listMulti n = map multiByTen n

--Q6
incrementEl e = map (++"IFmmp")e

--Q7
intList = filter (\n -> mod n 42 == 0)
ifDivisibleBy42 n
    | (length (intList n))>0 =True
    | otherwise =False 

--Q8
raise num = zipWith(**)(replicate (length num)10)num

--Q9
removeSpace = takeWhile (/=' ')

--Q10
evenList = filter (\n -> mod n 2 == 0)
onlyEven n
    | (length (evenList n))==(length(n)) =True
    | otherwise =False

--Q11
addNot x = "not "++x
addToList x = map addNot x

--Q12
reverseStr x =reverse x
reverseList x =map reverseStr x

{-
   Question 13 - 18:  urewrite as lambda functions
   Result will be printed in the main method.
-}

--Q1
--Trace out the execution of each expression, as was done in class, and show how they are different.

--Q 2-4
{-
call the function in the module Assn2b in main method
-}

main = do
        --Q1
        print(gcdCheck 111 259)
        print(gcdCheck 2945 123042)
        print(gcdCheck (2*5*7)(7*23))

        --Q2
        print([fibonacci n | n <- [0..20]])
        
        --Q3
        print(count 7 [1,7,6,2,7,7,9])
        print(count 'w' "western oregon wolves")
        
        --Q4
        print(sanitize "http://wou.edu/my homepage/I love spaces.html")
        
        --Q5
        print(listMulti [1,2,3,4,5,6])
        
        --Q6
        print(incrementEl ["Hello","Test","Not"])
        
        --Q7
        print(ifDivisibleBy42 [42,43,64])
        print(ifDivisibleBy42 [43])
        
        --Q8
        print(raise [5,3,8,2,3,6,3])
        
        --Q9
        print(removeSpace "HelloWorld ")
        
        --Q10
        print(onlyEven [5,3,8,2,3,6,3])
        print(onlyEven [4,6,8,2])
        
        --Q11
        print(addToList ["Funny","cold","slow"])
        
        --Q12
        print(reverseList ["This","is","a","sentence"])
        
        --Q13
        print((\x y -> x+y)3 4)
        
        --Q14
        print((\x->x*4)3)

        --Q15
        print((\y->y!!1)[1,2,3,4])
        
        --Q16
        print((\x->round(sqrt x))9933)
        
        --Q17
        print((\y->words y)"This is a sentence written in the usual way.")
        
        --Q18
        print(zip[(3,4),(5,16),(9.4,2)] (map(\x->sqrt ((fst x)**2+(snd x)**2))[(3,4),(5,16),(9.4,2)]))
        
        --Q2
        print(ownLength [1,2,3,4,5])
        
        --Q3
        print(convertIntToStringLeft [5,2,8,3,4])
        print(convertIntToStringRight [5,2,8,3,4])
        
        --Q4
        print(rewriteOne)
        print(rewriteTwo)